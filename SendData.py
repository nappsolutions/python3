# -*- coding: utf-8 -*-

# bibliotecas
import datetime
import json
import requests
import pandas as pd
import time
from requests.auth import HTTPBasicAuth

# url do servidor (disponibilizado na documentação)
url = 'http://xxxx.nappsolutions.com/service/receiver'

# lista de vendas
orders =[]

# Converter data para milissegundos
def convertToMilliseconds(dt):
	t =  pd.Timestamp(dt)
	return int(time.mktime(t.timetuple())) * 1000

# idLoja = ID fornecido pela Napp
# nomeLoja = Nome da Loja
# pedidos = Array de pedidos com codigo, valor total, data da venda

orders.append({"pedidoCode":"10001","valorTotal":10.0,"dataPedido": convertToMilliseconds('2017-09-06 10:00:00')})
orders.append({"pedidoCode":"10002","valorTotal":100.0,"dataPedido": convertToMilliseconds('2017-09-06 12:00:00')})
orders.append({"pedidoCode":"10003","valorTotal":120.0,"dataPedido": convertToMilliseconds('2017-09-06 00:00:00')})
orders.append({"pedidoCode":"10004","valorTotal":230.0,"dataPedido": convertToMilliseconds('2017-09-06 00:00:00')})
orders.append({"pedidoCode":"10005","valorTotal":15.0,"dataPedido": convertToMilliseconds('2017-09-06 00:00:00')})

# Objeto que contém todas as vendas
order = {"idLoja":1, "nomeLoja":"Test", "pedidos": orders}

# construção do header
headers = {'content-type': 'application/x-www-form-urlencoded'}
 
ordersJson = json.dumps(order) # convert o objeto venda para o envio

# auth=('usuario', 'senha'), os dados são fornecidos pela Napp
r = requests.post(url, auth=('usuario', 'senha'), data='content=' + ordersJson, headers=headers) # cria o request com basic auth
print r.status_code # codigo de retorno: 200 OK, 401 erro de auth, 404 erro
print r.text # resposta do servido